FROM golang:1.11
EXPOSE 80
RUN ls -lah
RUN pwd
RUN ls -lah ./bin/hello-server
COPY ./bin/hello-server /usr/local/bin/
RUN ls -lah
ENV GOKUBE v55
CMD ["hello-server"]
